<?php

/**
 * @file
 * Administrative page callbacks for the userbar module.
 */

/**
 * Admin settings for Userbar
 */
function userbar_admin_settings() {

  //set the default refresh time for the site
  $form['userbar_refresh'] = array(
    '#type' => 'textfield',
    '#title' => t('Auto-refresh interval (s)'),
    '#size' => 5,
    '#description' => t('Set the auto-refresh interval in seconds.'),
    '#default_value' => _userbar_get_refresh_rate(FALSE)
  );
  $userbar_imgtype = variable_get('userbar_imgtype', 0);
  if ($userbar_imgtype) {
    //check if raphael module is installed
    if (!module_exists('raphael')) {
      drupal_set_message(t('SVG usage needs !module to be enabled. Enable the module before selecting SVG option.',
        array('!module' => l(t('Raphael'), 'http://drupal.org/project/raphael'))), 'error');
      variable_set('userbar_imgtype', 0);
      $userbar_imgtype = 0;
    }
  }
  $form['userbar_imgtype'] = array(
    '#type' => 'radios',
    '#title' => t('Select image type'),
    '#options' => array(0 => t('Images'), 1 => t('SVG')),
    '#description' => t('See README.txt for more information'),
    '#default_value' => $userbar_imgtype,
  );
  
  $form['userbar_svga'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use attribute information in SVG file'),
    '#default_value' => variable_get('userbar_svga', 1),
    '#description' => t('Deselect to override the SVG attribute information'),
    '#states' => array('visible' => array(':input[name="userbar_imgtype"]' => array('value' => '1'))),
  );

  $form['userbar_svga_fs'] = array(
    '#type' => 'fieldset',
    '#title' => t('SVG Attribute Values'),
    '#states' => array('visible' => array(':input[name="userbar_imgtype"]' => array('value' => '1'))),
  );
  //bullet fill & stroke
  $form['userbar_svga_fs']['cont-b'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['userbar_svga_fs']['cont-b']['userbar_svga_bfill'] = array(
    '#type' => 'textfield',
    '#title' => t('Bullet Fill'),
    '#default_value' => variable_get('userbar_svga_bfill', '#FF0000'),
    '#size' => 7,
    '#maxlength' => 7,
  );
  $form['userbar_svga_fs']['cont-b']['userbar_svga_bstroke'] = array(
    '#type' => 'textfield',
    '#title' => t('Bullet Stroke'),
    '#default_value' => variable_get('userbar_svga_bstroke', '#FFFFFF'),
    '#size' => 7,
    '#maxlength' => 7,
  );
  
  //icon fill and stroke
  $form['userbar_svga_fs']['cont-i'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('container-inline')),
    '#states' => array('visible' => array(':input[name="userbar_svga"]' => array('checked' => FALSE))),  
  );
  $form['userbar_svga_fs']['cont-i']['userbar_svga_fill'] = array(
    '#type' => 'textfield',
    '#title' => t('Icon Fill'),
    '#default_value' => variable_get('userbar_svga_fill', '#000000'),
    '#size' => 7,
    '#maxlength' => 7,
  );
  $form['userbar_svga_fs']['cont-i']['userbar_svga_stroke'] = array(
    '#type' => 'textfield',
    '#title' => t('Icon Stroke'),
    '#default_value' => variable_get('userbar_svga_stroke', '#000000'),
    '#size' => 7,
    '#maxlength' => 7,
  );


  $form['userbar_usedock'] = array('#type' => 'checkbox', '#title' => t('Dock Userbar'),
      '#default_value' => variable_get('userbar_usedock', 1),
      '#description' => t('Userbar will dock at the right bottom by default. The block will be disabled if SVG is chosen.'));
  //check if beautytips is installed if it is selected
  if (variable_get('userbar_beautytips', 0)) {
    if (!module_exists('beautytips')) {
      drupal_set_message(t('Beauty tips module is not enabled. Enable the module and then select %bt option', array('%bt' => t('Use beauty tips'))), 'error');
      variable_set('userbar_beautytips', 0); //reset so that the site does not crash 
    }
  }
  $form['userbar_beautytips'] = array('#type' => 'checkbox', '#title' => t('Use beautytips'),
      '#default_value' => variable_get('userbar_beautytips', 0),
      '#description' => t('Userbar will use the beauty tips module !module to display status messages.', array('!module' => l(t('Beauty tips'), 'http://drupal.org/project/beautytips'))));
  //show selection screen for beautytips styles
  if (variable_get('userbar_beautytips', 0)) {
    $form['userbar_bt_fs'] = array('#type' => 'fieldset', '#title' => t('Beautytips settings'));
    $styles = beautytips_get_styles();
    foreach ($styles as $key => $value) {
      $options[$key] = $key;
    }
    $form['userbar_bt_fs']['userbar_beautytips_style'] = array('#type' => 'select', '#title' => t('Select beautytips style'),
      '#options' => $options, '#default_value' => variable_get('userbar_beautytips_style', $key));
    $options = array('bottom' => t('Bottom'), 'top' => t('Top'), 'left' => t('Left'), 'right' => t('Right'));
    $form['userbar_bt_fs']['userbar_beautytips_dock'] = array('#type' => 'select', '#title' => t('Position for dockbar'),
      '#options' => $options, '#default_value' => variable_get('userbar_beautytips_dock', 'top'));
    $form['userbar_bt_fs']['userbar_beautytips_block'] = array('#type' => 'select', '#title' => t('Position for block'),
      '#options' => $options, '#default_value' => variable_get('userbar_beautytips_block', 'bottom'));
  }
  return system_settings_form($form);
}
