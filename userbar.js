(function($) {
  $(document).ready(function() {
    if (Drupal.settings.userbar.is_admin) {
	   //if dockbar is shown, hide it.
	     if ($('#userbar_dock').length) {
	       $('#userbar_dock').html('');
	       $('#userbar_dock').hide();
	     }
	     if ($('#userbar_block').length) {
	       $('#userbar_block').html('');
	       $('#userbar_block').hide();
	     }
	  }
	  else {
      setTimeout(Drupal.refreshUserbar,Drupal.settings.userbar.refreshRate[0]);
	  }
    Drupal.loadUserbarSVG();
  });//end of document ready
  
  var attachBeautyTips = function() {
    if (typeof Drupal.behaviors.beautytips != 'undefined') {
    	Drupal.behaviors.beautytips.attach();
    }
  }
  
  Drupal.loadUserbarData = function(data) {
    //if the dock is available set its contents
    if ($('#userbar_dock').length) {
      $('#userbar_dock').html(data['data']['#markup']);
    }
    //if block is available set its contents
    if ($('#userbar_block').length) {
      $('#userbar_block').html(data['data']['#markup']);
    }
    Drupal.loadUserbarSVG();
    attachBeautyTips();
  //set the time out for next refresh
    setTimeout(Drupal.refreshUserbar, Drupal.settings.userbar.refreshRate[0]);
  };
  
  Drupal.loadUserbarSVG = function() {
    if (typeof Drupal.settings.userbar.svgdata != 'undefined') {
      var paperWidth = "24px";
      var counterWidth = "20px";
      var center = 10;
      for (var svgdata in Drupal.settings.userbar.svgdata[0]) {
        if ($('#' + svgdata).length > 0 ) {
          var paper = Raphael(svgdata, paperWidth, paperWidth);
          for (var pathdata in Drupal.settings.userbar.svgdata[0][svgdata]['path']) {             
            var img = paper.path(Drupal.settings.userbar.svgdata[0][svgdata]['path'][pathdata]['d']);
            if (typeof Drupal.settings.userbar.svgdata[0][svgdata]['path'][pathdata]['fill'] != 'undefined') {
              img.attr('fill', Drupal.settings.userbar.svgdata[0][svgdata]['path'][pathdata]['fill']);
            }
            if (typeof Drupal.settings.userbar.svgdata[0][svgdata]['path'][pathdata]['stroke'] != 'undefined') {
              img.attr('stroke', Drupal.settings.userbar.svgdata[0][svgdata]['path'][pathdata]['stroke']);
            }
            else {
              img.attr('stroke', '');
            }
          }
          if ($('#' + svgdata + ' .with-counter').length > 0){
            var txt = $('#' + svgdata + ' .with-counter').text();
            $('#' + svgdata + ' .with-counter').text('');
            var withElement = $('#' + svgdata + ' .with-counter').get();
            var cpaper = Raphael(withElement[0], counterWidth, counterWidth);            
            cpaper.circle(center, center, (center - 1)).attr({fill:Drupal.settings.userbar.svgdata[0]['bullet']['fill'],
              stroke:Drupal.settings.userbar.svgdata[0]['bullet']['fill']});
            cpaper.circle(center, center, (center - 1 - Drupal.settings.userbar.svgdata[0]['bullet']['stroke-width'])).attr(Drupal.settings.userbar.svgdata[0]['bullet']);
            cpaper.text(center, center, txt);
          }
        }
      }
    }
  };
  
  Drupal.refreshUserbar = function() {
    $.getJSON('/userbar/refresh', null, Drupal.loadUserbarData);
  };
})(jQuery);
